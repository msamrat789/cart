FROM       node:alpine3.14
RUN        apk update && apk add --no-cache bash && \
           rm -rf /var/cache/apk/* && mkdir app
WORKDIR    /app
COPY       server.js .
COPY       node_modules/ /app/node_modules/
COPY       run.sh /
ENTRYPOINT [ "bash", "/run.sh" ]